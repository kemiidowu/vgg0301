

function generateRandId() {
 return new Date().getTime().toString() + Math.floor(Math.random() * 1000000);
}

const adminForm = document.forms["admin_form"];


adminForm.addEventListener("submit", adminReg, false);

function adminReg(e) {
  e.preventDefault();
  loading()

  const collectData = {};
  

  const arrayData = [];
  collectData.name = adminForm.formName.value;
  collectData.title = adminForm.formTitle.value;
  collectData.location = adminForm.theLocation.value;
  collectData.type = adminForm.jobType.value;
  collectData.salary = adminForm.jobSalary.value;
  collectData.start = adminForm.startDate.value;
  collectData.close = adminForm.closeDate.value;
  collectData.details= adminForm.jobDetails.value;
  collectData.description = adminForm.jobDescription.value;
  collectData.id= generateRandId() ;


  
  if (localStorage.getItem("adminData") !== null){

  
    let getAdminValue = JSON.parse(localStorage.getItem("adminData"));

    getAdminValue.push(collectData);
    saveJobs(JSON.stringify(getAdminValue));
    window.location.reload()

  }
  else if (localStorage.getItem("adminData") == null) {
    
    arrayData.push(collectData);
    saveJobs(JSON.stringify(arrayData));

    

  }
  return collectData;

}

  function saveJobs(data){
    localStorage.setItem("adminData", data);

    
}

function getNumberOfJobs(){
    let adminData = localStorage.getItem("adminData");
    if(adminData){
        let parsedAdminData = JSON.parse(adminData);
        return parsedAdminData.length ;
    }else{
        return 0;
    }
}

function renderNumberOfJobs(){
    document.querySelector('#jobNumbers').innerHTML = getNumberOfJobs();
}

displayJobCategories()


