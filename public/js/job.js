function renderJobCategories() {
  let selectElement = document.querySelector("#jobTitle");

  let categories = getJobCategories();
  if (categories) {
    for (let i = 0; i < categories.length; i++) {
      let currentCategory = categories[i];
      let option = document.createElement("option");
      option.setAttribute("value", currentCategory);
      option.innerHTML = currentCategory;
      selectElement.appendChild(option);
    }
  } else {
    let option = document.createElement("option");
    option.setAttribute("value", 0);
    option.innerHTML = "No Job titles";
    selectElement.appendChild(option);
    // selectElement.disabled;
  }
}

renderNumberOfJobs();
renderJobCategories();

// form for the single field

const searchForm = document.forms["search"];

document.querySelector("#jobFilter").addEventListener("submit", mainSearch);

searchForm.addEventListener("submit", jobSearch, false);

function jobSearch(e) {
  e.preventDefault();

  const collectSearch = searchForm.focus.value;

  jobFilter(collectSearch);
}

// for searching a single field

function jobFilter(collectedSearch) {
  let theData = JSON.parse(localStorage.getItem("adminData"));

  let search = collectedSearch.trim().toLowerCase();

  if (search.length > 0) {
    theData = theData.filter(function(obj) {
      return Object.keys(obj).some(function(key) {
        return obj[key].toLowerCase().includes(search);
      });
    });
  }
  return filteredJob(theData);
}

// for the search bar

function mainSearch(e) {
  e.preventDefault();
  const jobCompanyValue = document.querySelector("#company").value.trim();
  const jobTitleValue = document.querySelector("#jobTitle").value.trim();
  const filteredJobs = [];

  if (
    (jobCompanyValue || jobCompanyValue != "") &&
    (jobTitleValue || jobTitleValue != "")
  ) {
    const jobData = readFromLocalStorage();
    if (jobData && jobData.length > 0) {
      for (let i = 0; i < jobData.length; i++) {
        let currentJob = jobData[i];
        console.log("*********Iteration" + (i + 1) + "*********");
        console.log(
          "Title from localstorage: " + currentJob.title.toLowerCase()
        );
        console.log("Title from form: " + jobTitleValue);
        console.log("\n");
        console.log(
          "name  from localstorage: " + currentJob.name.toLowerCase()
        );
        console.log("Location from form: " + jobCompanyValue);
        console.log("*********End iteration*********");
        console.log("\n\n");
        if (
          currentJob.name
            .toLowerCase()
            .includes(jobCompanyValue.toLowerCase()) &&
          currentJob.title.toLowerCase().includes(jobTitleValue.toLowerCase())
        ) {
          filteredJobs.push(currentJob);
          console.log(filteredJobs);

          filteredJob(filteredJobs);

          function renderNumberOfJobs() {
            if (filteredJobs) {
              let searchedData = filteredJobs.length;

              return (document.querySelector(
                "#jobNumbers"
              ).innerHTML = searchedData);
            } else {
              return (document.querySelector("#jobNumbers").innerHTML = "0");
            }
          }
          renderNumberOfJobs();
        } else {
          return false;
        }
      }
    } else {
      console.log(filteredJobs);

      let list = "";

      // let currentJob = postedData;
      list += "<div class=noJob>No Job Found</div>";

      document.querySelector("#jobList").innerHTML = list;
    }
  } else {
    console.log(filteredJobs);

    let list = "";

    // let currentJob = postedData;
    list += "<div class=noJob>Invalid</div>";
    document.querySelector("#jobList").innerHTML = list;
  }
}

// parsed data from local storage

function readFromLocalStorage() {
  let dataStore = localStorage.getItem("adminData");
  if (dataStore) {
    // check if data exists
    return JSON.parse(dataStore);
  } else {
    return false;
  }
}

function displayJobCategories() {
  let adminData = localStorage.getItem("adminData"); // returns undefined if adminData does not exist in localStorage
  if (adminData) {
    let list = "";
    let parsedAdminData = JSON.parse(adminData);
    for (let i = 0; i < parsedAdminData.length; i++) {
      let currentJob = parsedAdminData[i];

      list +=
        `<div class=single-post><div class=jobName> <h4>` +
        currentJob["title"] +
        `</h4></div><div class= jobDetails><h6><i class="fa fa-briefcase"></i>` +
        currentJob["name"] +
        `</h6><a href=apply.html class=view>Apply</a><p class=detail>` +
        currentJob["details"] +
        `</p><p class=address ><i class="fa fa-map-marker"></i>` +
        currentJob["location"] +
        `</p><p class=salary><i class="fas fa-money-check-alt"></i>` +
        currentJob["salary"] +
        `</p><p class=application-deadline ><span> Closing Date: </span> ` +
        currentJob["close"] +
        `</p></div></div>`;

      document.querySelector("#jobList").innerHTML = list;
    }
  } else {
    return undefined; // false
  }
}

displayJobCategories();
