function getNumberOfJobs(){
    let adminData = localStorage.getItem("adminData");
    if(adminData){
        let parsedAdminData = JSON.parse(adminData);
        return parsedAdminData.length ;
    }else{
        return 0;
    }
}

// function rendering number of Jobs 
function renderNumberOfJobs(){
    // console.log(document.querySelector('#jobNumbers'));
    document.querySelector('#jobNumbers').innerHTML = getNumberOfJobs();
}
 
    let adminData = localStorage.getItem("adminData");
    let parsedData = JSON.parse(adminData);
// Drop down input for job search.
function getUnique(arr, comp) {
 
    const unique = arr
    .map(e => e[comp])
    
    // store the keys of the unique objects
    .map((e, i, final) => final.indexOf(e) === i && i)
    
    // eliminate the dead keys & store unique objects
    .filter(e => arr[e]).map(e => arr[e]);
    
    return unique;
   }
 


function getJobCategories(){
    let uniqueParsedData = getUnique(parsedData,'title');
    let categories = [];
    // let adminData = localStorage.getItem("adminData");  // returns undefined if adminData does not exist in localStorage
    if(uniqueParsedData){

        for( let i = 0 ; i < uniqueParsedData.length ; i++ ){
            let currentJob = uniqueParsedData[i];
            
            if(currentJob.hasOwnProperty('title')){
                categories.push(currentJob.title);
                console.log(currentJob)

            }else{
                return 0; // Falsy values => [ "" | 0 | "0" | false | undefined | null ]
            }

        }
        
        return categories ;
    }else{
        return undefined; // false
    }
}


