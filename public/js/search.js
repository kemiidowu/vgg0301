function filteredJob(filteredJobs) {
  if (Array.isArray(filteredJobs) && filteredJobs.length) {
    let list = "";
    for (let i = 0; i < filteredJobs.length; i++) {
      let searchedJob = filteredJobs[i];

      // let currentJob = postedData;

      console.log(searchedJob);
      list +=
        `<div class=single-post><div class=jobName> <h4>` +
        searchedJob["title"] +
        `</h4></div><div class= jobDetails><h6><i class="fa fa-briefcase"></i>` +
        searchedJob["name"] +
        `</h6><a href=apply.html class=view>Apply</a><p class=detail>` +
        searchedJob["details"] +
        `</p><p class=address ><i class="fa fa-map-marker"></i>` +
        searchedJob["location"] +
        `</p><p class=salary><i class="fas fa-money-check-alt"></i>` +
        searchedJob["salary"] +
        `</p><p class=application-deadline ><span> Closing Date: </span> ` +
        searchedJob["close"] +
        `</p></div></div>`;
    }

    document.querySelector("#jobList").innerHTML = list;
  } else {
    // let currentJob = postedData;

    document.querySelector(
      "#jobList"
    ).innerHTML = `<div class=noJob>No Job Found</div>`;
  }
}
